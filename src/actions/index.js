import Axios from 'axios';

const API_KEY = "cd3f2f2640146b52d10e962f52457012";
const URL_BASE = `http://api.openweathermap.org/data/2.5/forecast/?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export let fetchWeather = (city) => {
  const url = `${URL_BASE}&q=${city},de`; // static country code, TODO: refactor to allow country code
  return {
    type: FETCH_WEATHER,
    payload: Axios.get(url)
  }
}