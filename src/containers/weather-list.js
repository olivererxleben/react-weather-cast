import React from 'react';
import { connect } from 'react-redux';

import Chart from '../components/chart';
import GoogleMap from '../components/google-map';

class WeatherList extends React.Component {

  // each row
  renderWeather = (cityData) => {
    const name = cityData.city.name
    const temps = cityData.list.map( weather => weather.main.temp ).map( temp => temp - 273)
    const pressures = cityData.list.map( weather => weather.main.pressure )
    const humids = cityData.list.map( weather => weather.main.humidity )

    return (
      <tr key={name}>
        <td className="city"><GoogleMap lat={cityData.city.coord.lat} lon={cityData.city.coord.lon} /></td>
        <td><Chart data={temps} lineColor="orange" width={180} height={120} unit="°C"/></td>
        <td><Chart data={pressures} lineColor="blue" width={180} height={120} unit="hPA" /></td>
        <td><Chart data={humids} lineColor="green" width={180} height={120} unit="%"/></td>
        <td></td>
      </tr>
    )
  }

  render() {
    return (

      <table className="table table-hover">
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature</th>
            <th>Pressure</th>
            <th>Humidity</th>
            
          </tr>
        </thead>
        <tbody>
          { this.props.weather.map( this.renderWeather) } 
        </tbody>
      </table>
    )
  }
}

export default connect(
  // map State to Props
  (state) => ({weather: state.weather})
  
)(WeatherList)