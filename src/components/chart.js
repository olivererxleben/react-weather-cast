import React from 'react';
import _ from 'lodash';
import {Sparklines, SparklinesLine, SparklinesReferenceLine} from 'react-sparklines';

export default (props) => {
  
  const average = () => {
    return _.round(_.sum(props.data)/props.data.length);
  }

  return ( 
    <div>
      <Sparklines svgWidth={props.width} svgHeight={props.height} data={props.data} >
        <SparklinesLine color={props.lineColor}/>
        <SparklinesReferenceLine type="avg" />
      </Sparklines>
      <div>
        avg: {average()} {props.unit}
      </div>
    </div>
  )
}