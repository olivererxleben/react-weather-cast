import {FETCH_WEATHER} from '../actions/index';

export default (state = [], action) => {

  console.log('action received', action);

  switch(action.type) {
    case FETCH_WEATHER:
      // never manipulate state directly!!! Correct approach here: return a new array with new entry and old entries. 
      // IMMUTABILITY FTW
      return [action.payload.data, ...state];
  }
  return state;
}